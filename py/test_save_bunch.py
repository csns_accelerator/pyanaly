'''
Albert Hu
Python testing file
Conversion of bunches to HDF5 file
'''


import h5py
import numpy as np
from pathlib import Path
from test_file_parser import Parser
import time
import os


class bunch:
    """
    input: infolder for multipule or single data folder,
    outfolder for dst that you want to save your h5
    files after processing.
    """

    def __init__(self, infolder, outfolder):
        self.in_folder = infolder
        self.out_folder = outfolder
        self.part_attr = {}
        self.part_attr_name = []
        self.bunch_type = "survive"
        self.position = None
        self.position_attr = []
        if self.in_folder:
            self.read_dir()

    def read_dir(self):
        p = Path(self.in_folder)
        project_count = 0
        for dir in p.iterdir():
            self.data_dir = dir
            self.new_hdf(dir, str(project_count))
            project_count += 1

    def read_file(self, bunches_group):
        '''get all bunch file and return to file_process'''
        for file in Path(self.data_dir).rglob("*.bunch"):
            self.file_parser(file)
            self._save_to_group(bunches_group)
            self.part_attr = {}
            self.position_attr = []
            self.bunch_type = "survive"
            self.part_attr_name = []
            self.position = None

    def file_parser(self, in_file):
        '''Prase data from bunch file'''
        f = open(in_file, 'r')
        with open(in_file, "r") as f:
            line_count = 0
            for line in f:
                if line.startswith('%'):
                    line_count += 1
                    line = line.replace(',', '')
                    line = line.replace(':', '')
                    line = line.replace('(', '')
                    line = line.replace(')', '')
                    line = line.replace('=', ' ')
                    line = line.split()
                    if line[1] == "BUNCH_ATTRIBUTE_DOUBLE":
                        self.part_attr[(line[2])] = float(line[3])
                    elif line[1] == "BUNCH_ATTRIBUTE_INT":
                        self.part_attr[(line[2])] = int(line[3])
                    elif line[1] == "PARTICLE_ATTRIBUTES_CONTROLLERS_NAMES":
                        self.part_attr_name = line[2:]
                    elif line[1] == "PARTICLE_ATTRIBUTES_CONTROLLER_DICT":
                        self.bunch_type = "lost"
                    elif (line[1] == "SYNC_PART_COORDS" or
                          line[1] == "SYNC_PART_MOMNTUM" or
                          line[1] == "SYNC_PART_X_AXIS"):
                        self.part_attr[(line[5])] = float(line[2])
                        self.part_attr[(line[6])] = float(line[3])
                        self.part_attr[(line[7])] = float(line[4])
                    elif line[1] == "SYNC_PART_TIME":
                        self.part_attr[(line[3])] = float(line[2])
                    elif line[1] == "info":
                        self.part_attr[(line[3])] = float(line[-1])
                    elif line[1] == "x[m]":
                        self.position_attr = line
                        self.position = np.loadtxt(
                                                   in_file,
                                                   skiprows=line_count,
                                                   dtype=float)
                    continue

    def new_hdf(self, dir, file_name):
        '''create new hdf file and group'''
        id = file_name
        file_name = str(self.out_folder) + '/' + file_name
        with h5py.File(file_name, 'w') as hdf_file:
            bunches_group = hdf_file.create_group('bunch')
            twiss_group = hdf_file.create_group('twiss')
            log_group = hdf_file.create_group('log')
            config_gorup = hdf_file.create_group('config')
            p = Parser(dir, twiss_group, log_group, config_gorup, hdf_file)
            p.search_file()
            self.read_file(bunches_group)
            author = hdf_file["config/config/author"][()].decode()
            date = hdf_file["config/config/date"][()]
            turn = hdf_file["config/track/turn"][()]
            date = time.strftime("%Y-%m-%d_%H:%M:%S", time.localtime(date))
            os.rename(file_name, self.out_folder + "/" + author + "_" + date + "_" + str(id) + ".h5")
        hdf_file.close()

    def _save_to_group(self, bunches_group):
        '''save nparray to dataset and save part_attr to attribute'''
        dataset = bunches_group.create_dataset(self.bunch_type + "_" +
                                               str(self.part_attr["Turn"]),
                                               data=self.position)
        for k, v in self.part_attr.items():
            dataset.attrs[k] = v
        dataset.attrs["position_attrs"] = self.position_attr

