'''
Albert HU
Combining multipule classes
'''

import numpy as np
from pathlib import Path
import json
from silx.io.dictdump import dicttoh5
import time

class Parser():
    '''parse all file except .bunch and write to hdf file'''

    def __init__(self, infolder, twiss_group, log_group, config_group, h5_file):
        self.in_folder = infolder
        self._twiss_group = twiss_group
        self._log_group = log_group
        self._config_group = config_group
        self._twiss_dump_position = None
        self._pt_attr = None
        self.h5_file = h5_file
        # self.bpm_gorup = bpm_group

    def search_file(self):
        '''search file from dir and pass to parser'''
        p = Path(self.in_folder)
        self._parse_log(p.rglob("*.out"))
        self._parse_config(p.rglob("*.json"))
        self._parse_twiss(p.rglob("*.dat"))
        # self._parse_bpm()

    def get_name(self):
        name = self._config_file["config"]['author']
        date = self._config_file["config"]['date']
        return name + "_" + str(date)

    def _parse_config(self, file):
        for file in file:
            with open(file, "r") as json_file:
                self._config_file = json.load(json_file)
            self._pt_attr = self._config_file['painting']
            self._rf_attr = self._config_file['rfcavity']
            if self._pt_attr['enable'] is True:
                with open(self._pt_attr['curve'], "r") as ex_file:
                    self._pt_curve = np.loadtxt(ex_file)
            if self._rf_attr['enable'] is True:
                with open(self._rf_attr['curve'], 'r') as ex_file:
                    r = ex_file.readline()
                    self._rf_attr['rf_attr'] = r
                    self._rf_curve = np.loadtxt(ex_file)
        self._config_to_hdf()

    def _parse_log(self, file):
        '''parse log file'''
        self._log_data = []
        for file in file:
            with open(file, "r") as log_file:
                for line in log_file:
                    line = line.replace(",", "")
                    split_line = line.split()
                    if "aperture" in split_line:
                        if "design" in split_line:
                            self.aperture_type = "design"
                        elif "empeirical" in split_line:
                            self.aperture_type = "empeirical"
                    elif "magnets" in split_line:
                        if "AC" in split_line:
                            self.mag_run_type = "AC"
                        elif "DC" in split_line:
                            self.mag_run_type = "DC"
                    elif "rf" in split_line:
                        if "AC" in split_line:
                            self.rf_run_type = "AC"
                        elif "DC" in split_line:
                            self.rf_run_type = "DC"
                    elif "correctors" in split_line:
                        if "AC" in split_line:
                            self.cor_run_type = "AC"
                        elif "DC" in split_line:
                            self.cor_run_type = "DC"
                    elif "Total" in split_line:
                        self.track_time = split_line[3]
                    elif "Track" in split_line:
                        track_data = [0] * 4
                        track_data[0] = float(split_line[3])
                        track_data[1] = float(split_line[5])
                        track_data[2] = float(split_line[8])
                        track_data[3] = float(split_line[12])
                        self._log_data.append(track_data)
        self._log_data = np.array(self._log_data)
        self._log_to_hdf()

    def _parse_twiss(self, files):
        for file in files:
            self._twiss_dump_position = Path(file).stem.lstrip("stat_")
            self._twiss_attr_name = []
            self._twiss_data = None
            with open(file, "r") as file:
                line = file.readline()
                line = line.replace("#", "")
                line.split()
                self._twiss_attr_name = line
                self._twiss_data = np.loadtxt(file, skiprows=0, dtype=float)
        self._twiss_to_hdf()

    def _parse_bpm(self):
        '''false for now'''
        pass

    def _config_to_hdf(self):
        _rf = self._config_group.create_dataset('rfcavity_data',
                                                data=self._rf_curve)
        _pt = self._config_group.create_dataset('painting_data',
                                                data=self._pt_curve)
        for k, v in self._rf_attr.items():
            _rf.attrs[k] = v
        for k, v in self._pt_attr.items():
            _pt.attrs[k] = v
        dicttoh5(self._config_file, self.h5_file, h5path="/config", overwrite_data=None)

    def _twiss_to_hdf(self):
        '''send parsed twiss dset to hdf'''
        _twiss_dset = self._twiss_group.create_dataset("twiss/" +
                                                       self._twiss_dump_position,
                                                       data=self._twiss_data)
        _twiss_dset.attrs['twiss_attr'] = self._twiss_attr_name

    def _log_to_hdf(self):
        '''send parsed log file to hdf'''
        _log_dset = self._log_group.create_dataset("log", data=self._log_data)
        _log_dset.attrs["mag_run_type"] = self.mag_run_type
        _log_dset.attrs["cor_run_type"] = self.cor_run_type
        _log_dset.attrs["rf_run_type"] = self.rf_run_type
        _log_dset.attrs["aperture_type"] = self.aperture_type
        _log_dset.attrs["data_name"] = ['Track on Turn', 'cost[s]',
                                        'time remain[s]',
                                        'particles remain']
        _log_dset.attrs['track_time'] = self.track_time
