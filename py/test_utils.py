"""
albert hu
uilts for ui
"""
import pandas as pd
import os
import h5py
import pathlib
from silx.io.dictdump import h5todict
import time
import sys
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import Qt
import numpy as np

def read_h5_config(in_folder):
    """
    read h5 file and turn multipule set of
    config information to a single table
    """
    main_config = []
    author = []
    date = []
    turn = []
    h5_file = []
    for file in list(pathlib.Path(in_folder).glob("*.h5")):
        with h5py.File(file, 'r') as hdf_file:
            a = h5todict(hdf_file, "/config")
            author.append(str(a["config"]["author"]))
            date.append(time.strftime("%Y-%m-%d_%H:%M:%S", time.localtime(float(a["config"]["date"]))))
            turn.append(int(a["track"]["turn"]))
            h5_file.append(str(pathlib.Path(file)))
    main_config = [author, date, turn, h5_file]
    header = ["Author", "Date", "Turn", "File_path"]
    return [list(i) for i in zip(*main_config)], header

def load_h5(h5_path):
    with h5py.File(h5_path,"r") as hdf_file:
        pass
        
def read_turns(h5_path):
    with h5py.File(h5_path,"r") as hdf_file:
        return(list(hdf_file["bunch"].keys()))

def get_bunch(h5_path, turn, orien):
    print("gatherings_data")
    with h5py.File(h5_path,"r") as hdf_file:
        x = []
        y = []
        xp = []
        yp = []
        z = []
        dE = []
        for array in hdf_file['bunch'][turn][()]:
            x.append(array[0])
            xp.append(array[1])
            y.append(array[2])
            yp.append(array[3])
            z.append(array[4])
            dE.append(array[5])
        """Hx = np.histogram(x, bins=256)
        Hxxp = np.histogram2d(x, xp, bins=256)
        Hy = np.histogram(y, bins=256)
        Hyyp = np.histogram2d(y, yp, bins=256)
        Hz = np.histogram(z, bins=256)
        HzdE = np.histogram2d(z, dE, bins=256)
        Hx_y = np.histogram2d(x, y, bins=256)
        if orien == "X-xp":
            return(Hx, Hxxp)
        elif orien == "Y-yp":
            return(Hy, Hyyp)
        elif orien == "Z-dE":
            return(Hz, HzdE)
        elif orien == "X-Y":
            return(None, Hx_y)
        elif orien == "All":
            return(Hx,Hxxp,Hy,Hyyp,Hz,HzdE,Hx_y)"""
        if orien == "X-xp":
            return(x, xp)
        elif orien == "Y-yp":
            return(y, yp)
        elif orien == "Z-dE":
            return(z, dE)
        elif orien == "X-Y":
            return(x,y)
        elif orien == "All":
            return(x,xp,y,yp,z,dE)
 



class TableModel(QtCore.QAbstractTableModel):
    """
    config tableview seting
    """
    def __init__(self, data, header):
        super(TableModel, self).__init__()
        self._data = data
        self._headerdata = header

    def data(self, index, role):
        # Colors = QtGui.QColor(....)
        if role == Qt.DisplayRole:
            return self._data[index.row()][index.column()]
        """if role == Qt.DecorationRole:
            value = self._data[index.row()][index.column()]
            if isinstance(value, bool):
                if value:
                    return QtGui.QIcon('tick.png')
                return QtGui.QIcon('cross.png')
            if (isinstance(value, int) or isinstance(value, float)):
                value = int(value)    
                return QtGui.QColor(Colors.....)
            if isinstance(value, datetime):
                return QtGui.QIcon('calendar.png')  
            """

    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if role == Qt.DisplayRole and orientation == Qt.Horizontal:
            return self._headerdata[section]
        return QtCore.QAbstractTableModel.headerData(self, section, orientation, role)

    def rowCount(self, index):
        return len(self._data)

    def columnCount(self, index):
        return len(self._data[0])
