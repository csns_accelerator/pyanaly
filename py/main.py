"""
main program for ui
"""
from PyQt5 import QtWidgets
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.uic import loadUi
from pathlib import Path
import numpy as np
import sys
import requests
import os
from test_save_bunch import bunch
import shutil
import test_utils
from test_utils import TableModel
import plot_utils 

class Py_ana(QtWidgets.QMainWindow):

    def __init__(self):
        super().__init__()
        loadUi("py/py_ana.ui", self)
        self._plot_selection = []
        self._turn_selection = []
        self.cwd = os.getcwd()
        self._set_action()
        self._set_combobox
        self.bunch_radioButton_2.setChecked(True)
        self.bunch_radioButton_2.toggled.connect(lambda: self._set_combobox(self.bunch_radioButton_2))
        self.paintcurve_radioButton_2.toggled.connect(lambda: self._set_combobox(self.paintcurve_radioButton_2))
        self.rfcurve_radioButton_2.toggled.connect(lambda: self._set_combobox(self.rfcurve_radioButton_2))

    def _set_action(self):
        self.Config_button.clicked.connect(lambda: self.test_function_1())
        self.Plot_button.clicked.connect(lambda: self._plot_function())
        self.load_button.clicked.connect(lambda: self._load_data())
        self.clear_button.clicked.connect(lambda: self.Reset())
        self.Save_button.clicked.connect(lambda: self.test_function_5())

    def _set_combobox(self, btn):
        """
        set action in combobox
        """
        project_select = self._plot_selection
        self.plot_type = btn.text()
        orien_select = ["All","X-xp","Y-yp","Z-dE","X-Y"]
        if btn.text() == "Bunch":
            if btn.isChecked()==True:
                self.Project_box.clear()
                self.Project_box.addItems(project_select)
                self.Orien_box.clear()
                self.Orien_box.addItems(orien_select)
        elif btn.text() == "Painting Curve" or btn.text() == "RF Curve":
            if btn.isChecked()==True:
                self.Turn_Box.clear()
                self.Orien_box.clear()
                self.Project_box.clear()
                self.Project_box.addItems(project_select)
        self.Project_box.activated.connect(lambda: self.to_turn_box(self.Project_box.currentText(),btn.text()))


    def to_turn_box(self,project,btn):
        """
        set action in turn selection box
        """
        self.Turn_Box.clear()
        if btn == "Bunch":
            turns = test_utils.read_turns(project)
            self.Turn_Box.addItems(turns)


    def test_function_1(self):
        """
        Config button action, load config group and compare
        """
        pass

    def Plot(self):
        self.canvas = plot_utils.MyFigure(width=5,height=4,dpi=100)
        self.plotlayout.addWidget(self.canvas)
    
    def Reset(self):
        while self.plotlayout.count():
            child = self.plotlayout.takeAt(0)
            if child.widget():
                child.widget().deleteLater()
        self.canvas.axes.clear()
        self.canvas.axes2.clear()

    def _plot_function(self):
        """
        Plot button action, make plot from bunches group
        """
        if len(self._plot_selection) == 0: 
            indexes = self.Config_table.selectionModel().selectedRows()
            model = self.Config_table.model()
            role = Qt.DisplayRole
            for index in indexes:
                self._plot_selection.append(model.data(model.index(index.row(),3),role))
        else:
            if self.plot_type == "Bunch":
                self._plot_bunch()
            elif self.plot_type == "Config_curve":
                pass

    def _plot_bunch(self):
        """
        plot bunch as 2dhistogram
        """
        self.Plot()
        print("ploting")
        project_selection = self.Project_box.currentText()
        turn_selection = self.Turn_Box.currentText()
        or_selection = self.Orien_box.currentText()
        if or_selection == "All":
            (x,xp,y,yp,z,dE)= test_utils.get_bunch(project_selection, turn_selection, or_selection)
            self.canvas.plot_all_bunch(x,xp,y,yp,z,dE)
        else:
            x,xp = test_utils.get_bunch(project_selection, turn_selection, or_selection)
            self.canvas.plot_bunch(x,xp,or_selection)

    def _load_data(self):
        """
        Load button action, load project to working area, then select
        project to make plot or compare config, can only load one dir
        each time.
        """
        self._working_dir = QFileDialog.getExistingDirectory(self,
                                               "Select or create a folder to keep your result",
                                               self.cwd)
        self._data_files = []
        while True:
            files, filetype = QFileDialog.getOpenFileNames(self,
                                        "Select Your Data",
                                        self.cwd,
                                        "Hdf File(*.h5)")
            if len(files) == 0:
                print("selection canceled")
                break
        self._data_files.extend(files)
        for file in self._data_files:
            src = file.split("/")
            if not Path(self._working_dir + file).exist():
                shutil.copyfile(file, self._working_dir + "/" + src[-1])
        #bunch(self._working_dir, self._working_dir)
        self._mainwindow_table_view()

    def _mainwindow_table_view(self):
        data, header = test_utils.read_h5_config(self._working_dir)
        print(type(data))
        self.model = TableModel(data, header)
        self.Config_table.setModel(self.model)
        self.Config_table.horizontalHeader().setStretchLastSection(True)
        self.Config_table.clicked.connect(self.viewClicked)
        self.Config_table.setSelectionBehavior(QTableView.SelectRows)
        #self.Config_table.selectionModel(QAbstractItemView.MultiSelection)


    def viewClicked(self, clickedIndex): # wroking on it
        self._config_row = clickedIndex.row()
        self._config_model = clickedIndex.model()

    def test_function_4(self):
        """
        Clear button action, clear working area
        """
        pass

    def test_function_5(self):
        """
        Save button action, save workspace, plot, config comparision
        """
        pass


if __name__ == "__main__":
    app = QApplication([])
    window = Py_ana()
    window.show()
    app.exec()
