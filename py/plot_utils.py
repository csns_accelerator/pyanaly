"""
Ploting canvas and utils
"""

import matplotlib
matplotlib.use("Qt5Agg")
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar

from matplotlib.pyplot import Figure
from matplotlib.image import NonUniformImage


class MyFigure(FigureCanvas):
    def __init__(self, width=5, height=4, dpi=100):
        self.fig = Figure(figsize=(width, height), dpi=dpi, facecolor="white")
        super(MyFigure,self).__init__(self.fig)

    def plot_bunch(self,x,xp,label):
        print("ploting")
        gs = self.fig.add_gridspec(3,3)
        self.axes = self.fig.add_subplot(gs[0:2,:])
        x_label = label.split("-")[0]
        y_label = label.split("-")[1]
        """Hist = xxp[0]
        Hist = Hist.T
        xedges = xxp[1]
        yedges = xxp[2]
        self.axes.cla()
        self.axes.imshow(Hist, interpolation="nearest", origin="lower",cmap='jet',
                         extent=[xedges[0],xedges[-1],yedges[0],yedges[-1]],aspect='auto')"""
        self.axes.cla()
        self.axes.hist2d(x, xp, bins=256, norm=LogNorm())
        self.axes.set_xlabel(x_label)
        self.axes.set_ylabel(y_label)
        self.axes2 = self.fig.add_subplot(gs[-1,:])
        self.axes2.cla()
        self.axes2.hist(x,bins=256)
        self.axes2.set_xlabel(x_label)
    
    def plot_all_bunch(self, x, xp, y, yp, z, dE):
        print("plotting all")
        gs = self.fig.add_gridspec(5,4)
        self.axes0 = self.fig.add_subplot(gs[0,0:2])
        self.axes1 = self.fig.add_subplot(gs[0,2:4])
        self.axes2 = self.fig.add_subplot(gs[1,0:2])
        self.axes3 = self.fig.add_subplot(gs[1,2:4])
        self.axes4 = self.fig.add_subplot(gs[2,0:2])
        self.axes5 = self.fig.add_subplot(gs[2,2:4])
        self.axes6 = self.fig.add_subplot(gs[3,:])
        self.axes0.hist2d(z, dE, bins=256, norm=LogNorm())
        self.axes1.hist2d(x, xp, bins=256, norm=LogNorm())
        self.axes2.hist2d(y, yp, bins=256, norm=LogNorm())
        self.axes3.hist2d(x, y, bins=256, norm=LogNorm())
        self.axes4.hist(x, bins=256)
        self.axes5.hist(y, bins=256)
        self.axes6.hist(z, bins=256)
        self.axes0.set_xlabel("z(m)")
        self.axes0.set_ylabel("dE(GeV)")
        self.axes1.set_xlabel("x(m)")
        self.axes1.set_ylabel("xp(rad)")
        self.axes2.set_xlabel("y(m)")
        self.axes2.set_ylabel("yp(rad)")
        self.axes3.set_xlabel("x(m)")
        self.axes3.set_ylabel("y(m)")
        self.axes4.set_xlabel("x(m)")
        self.axes5.set_xlabel("y(m)")
        self.axes6.set_xlabel("z(m)")
        self.fig.subplots_adjust(wspace=1, hspace=1)


